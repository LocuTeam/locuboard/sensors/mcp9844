## Základní použití driveru pro MCP9844

Vytvořená knihovna disponuje několika funkcemi, které umožňují správu, nastavení a čtení dat ze senzoru.

Příklad použití knihovny:

```c
// Inkludování potřebných knihoven
#include "složka/mcp9844.h" // Inkludování knihovny pro daný senzor MCP9844

mcp9844_t mcp9844; // Vytvoření struktury nesoucí veškeré informace o senzoru

// Vytvoření zapisovací funkce

// Vytvoření čtecí funkce

// Vytvoření funkce pro delay

int main(void){
    // Inicializace systémových knihoven
    
    mcp9844_init(&mcp9844, 0x18, user_i2c_write, user_i2c_read);
    
    mcp9844_config_get_default(&mcp9844);
    mcp9844_config_apply(&mcp9844);

    while(true){
        mcp9844_read_temperature(&mcp9844);
        printf("Temperature: %d.%02d\r\n", mcp9844.temp.integer, mcp9844.temp.decimal);
        user_delay_ms(500);
    }
    
    return 0;
}
```
